require 'test_helper'

class ForumPostsControllerTest < ActionController::TestCase
  setup do
    @forum_post = forum_posts(:one)
  end

  test "should create forum_post" do
    assert_difference('ForumPost.count') do
      post :create, forum_post: { user_id: @forum_post.user_id, body: @forum_post.body, thread_id: @forum_post.thread_id, title: @forum_post.title }
    end
  end

  test "should get edit" do
    get :edit, id: @forum_post
    assert_response :success
  end

  test "should update forum_post" do
    patch :update, id: @forum_post, forum_post: { user_id: @forum_post.user_id, body: @forum_post.body, thread_id: @forum_post.thread_id, title: @forum_post.title }
  end

  test "should destroy forum_post" do
    assert_difference('ForumPost.count', -1) do
      delete :destroy, id: @forum_post
    end
  end
end
