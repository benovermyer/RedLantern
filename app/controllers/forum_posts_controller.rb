class ForumPostsController < ApplicationController
  before_action :set_forum_post, only: [:show, :edit, :update, :destroy]

  # GET /forum_posts/1/edit
  def edit
  end

  # POST /forum_posts
  # POST /forum_posts.json
  def create
    @forum_post = ForumPost.new( forum_post_params )
    @forum_post.user = current_user

    respond_to do |format|
      if @forum_post.save
        if @forum_post.original_post
          @forum_post.thread_id = @forum_post.id
          @forum_post.save
        end
        format.html { redirect_to "/forums/#{@forum_post.category_id}/#{@forum_post.thread_id}", notice: 'Forum post was successfully created.' }
        format.json { render :show, status: :created, location: @forum_post }
      else
        format.html { render :new }
        format.json { render json: @forum_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forum_posts/1
  # PATCH/PUT /forum_posts/1.json
  def update
    respond_to do |format|
      if @forum_post.update(forum_post_params)
        format.html { redirect_to "/forums/#{@forum_post.category_id}/#{@forum_post.thread_id}", thread: @forum_post.thread_id, notice: 'Forum post was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_post }
      else
        format.html { render :edit }
        format.json { render json: @forum_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forum_posts/1
  # DELETE /forum_posts/1.json
  def destroy
    @forum_post.destroy
    respond_to do |format|
      format.html { redirect_to forum_posts_url, notice: 'Forum post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum_post
      @forum_post = ForumPost.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_post_params
      params.require(:forum_post).permit(:title, :author_id, :thread_id, :body, :category_id, :original_post)
    end
end
