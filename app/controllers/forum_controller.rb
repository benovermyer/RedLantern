class ForumController < ApplicationController
  before_action :authenticate_user!

  def index
    @categories = ForumCategory.all
  end

  def thread
    @posts = ForumPost.where( thread_id: params[ :thread ] )
    @thread = ForumPost.find( params[ :thread ] )
    @category = ForumCategory.find( @thread.category_id )
  end

  def category
    @threads = ForumPost.where( 'category_id = ? AND original_post = ?', params[ :category ], true )
    @category = ForumCategory.find( params[ :category ] )
  end

  def new
    @category = ForumCategory.find( params[ :category ] )
    @forum_post = ForumPost.new
    @forum_post.user_id = current_user.id
    @forum_post.category_id = params[ :category ]
    @forum_post.original_post = true
    render 'forum_posts/new'
  end

  def reply
    @category = ForumCategory.find( params[ :category ] )
    @forum_post = ForumPost.new
    @forum_post.user_id = current_user.id
    @forum_post.category_id = params[ :category ]
    @forum_post.original_post = false
    @forum_post.thread_id = params[ :thread ]
    render 'forum_posts/new'
  end
end
