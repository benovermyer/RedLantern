class ChangeAuthorIdToUserIdInForumPosts < ActiveRecord::Migration
  def change
    rename_column :forum_posts, :author_id, :user_id
  end
end
