class CreateForumPosts < ActiveRecord::Migration
  def change
    create_table :forum_posts do |t|
      t.string :title
      t.integer :author_id
      t.integer :thread_id
      t.text :body
      t.timestamps null: false
    end
  end
end
