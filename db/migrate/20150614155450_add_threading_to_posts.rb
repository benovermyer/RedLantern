class AddThreadingToPosts < ActiveRecord::Migration
  def change
    add_column :forum_posts, :original_post, :boolean
    add_column :forum_posts, :thread_id, :integer
    add_column :forum_posts, :category_id, :integer
    add_index :forum_posts, :thread_id
    add_index :forum_posts, :category_id
  end
end
