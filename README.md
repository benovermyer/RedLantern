# Red Lantern

This is a guild website content management system. It's intended specifically for the Crowfall guild "The Lantern Watch," but with a little effort (mostly styling), can be used for any guild.

[![Code Climate](https://codeclimate.com/github/BenOvermyer/RedLantern/badges/gpa.svg)](https://codeclimate.com/github/BenOvermyer/RedLantern)
[ ![Codeship Status for BenOvermyer/RedLantern](https://codeship.com/projects/33a675f0-f838-0132-f1f3-52c8a88014cc/status?branch=master)](https://codeship.com/projects/86561)

## Planned Features

* Rich user profiles
* Characters with game-specific metadata tied to users
* Intuitive roster interface with hierarchical editing for admins
* Simple and clean forums with customizable structure
* Real-time chat system
* Event calendar with attendance tracking
* Achievement/badge system integrated throughout the site
* Easy-to-use blogging
* Customizable media pages, including video and image galleries
