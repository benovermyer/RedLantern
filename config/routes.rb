Rails.application.routes.draw do
  resources :forum_categories
  resources :forum_posts
  resources :forum_threads

  devise_for :users
  resources :users

  get '/dashboard', to: 'users#dashboard'
  get '/forums', to: 'forum#index'
  get '/forums/:category', to: 'forum#category'
  get '/forums/:category/new', to: 'forum#new'
  get '/forums/:category/:thread', to: 'forum#thread'
  get '/forums/:category/:thread/reply', to: 'forum#reply'

  root to: "home#index"
end
